public class App {
    public static void main(String[] args) throws Exception {
        //This is a comment
        String appName = "DevCamp120 - Basic java";
        System.out.println("Hello, World! " + appName.length());
        System.out.println("toUpperCase: " + appName.toUpperCase());
        System.out.println("toLoweCase: " + appName.toLowerCase());
        /*
         * This is comment for multiline
         * line 1
         * line 2
         */
    }
}
